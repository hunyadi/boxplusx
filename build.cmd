@echo off
setlocal

pushd engine\css
node compile.js
if %errorlevel% neq 0 exit /b %errorlevel%
popd

pushd engine\typescript
call compile.cmd
if %errorlevel% neq 0 exit /b %errorlevel%
call tscc.cmd
if %errorlevel% neq 0 exit /b %errorlevel%
popd

if not exist engine\js mkdir engine\js
copy /y engine\typescript\boxplusx.js engine\js\boxplusx.js
copy /y engine\typescript\boxplusx.min.js engine\js\boxplusx.min.js
