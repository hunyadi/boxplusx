@echo off
rem Compiles the project both in debug and production mode.
rem
rem Debug mode produces a single JavaScript file by concatenating JavaScript output files
rem in the module import resolution order. This mode uses webpack.
rem
rem Production mode generates a single minified JavaScript file using the Google Closure Compiler
rem with advanced optimizations enabled.

rem Prerequisites:
rem npm install webpack webpack-cli typescript ts-loader

setlocal

rem Debug mode
call npx webpack
set TARGET_FILE=dist\boxplusx.js
set TEMP_FILE=%TARGET_FILE%.tmp
copy /b preamble.js + "%TARGET_FILE%" "%TEMP_FILE%"
del /q "%TARGET_FILE%"
move "%TEMP_FILE%" "%TARGET_FILE%"

rem Production mode
call tscc -- --project tsconfig.closure.json

exit /b
