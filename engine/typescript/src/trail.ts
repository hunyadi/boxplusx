/** Record pushed into the browser history. */
interface HistoryState<T> {
    uid?: string;
    item: T;
}

/** Converts an integer in the range [0, 255] into a hexadecimal representation. */
function dec2hex(dec: number): string {
    return ('0' + dec.toString(16)).slice(-2);
}

/** Generates a hexadecimal string representing n bytes of random data. */
function generateUID(len: number): string {
    const arr = new Uint8Array(len);
    window.crypto.getRandomValues(arr);
    return Array.from(arr, dec2hex).join('');
}

export default class Trail<T> {
    /** A unique ID that helps recognize history states managed by this instance. */
    private uid?: string;

    private eventFn: (event: PopStateEvent) => void;

    constructor(navigateFn: (item: T | undefined) => void) {
        // manage history (browser back and forward buttons)
        this.eventFn = (event: PopStateEvent) => {
            let item = undefined;
            if (this.isHistoryState(event.state)) {
                item = event.state.item;
            }
            navigateFn(item);
        };
    }

    /**
     * Gets the currently shown item.
     * @return The item currently displayed.
     */
    public get current(): T {
        const state = history.state as HistoryState<T>;
        return state.item;
    }

    /**
     * Pushes an item on top of the history stack.
     * @param item The item to be currently displayed.
     */
    public push(item: T): void {
        if (this.isHistoryState(history.state)) {
            if (item != this.current) {
                this.pushHistoryState(item);
            }
        } else {
            this.uid = generateUID(16);
            this.addEventListener();
            this.pushHistoryState(item);
        }
    }

    /** Removes the managed history stack. */
    public clear(): void {
        this.removeEventListener();
        this.unroll();
    }

    /** Discards all history items injected by this instance. */
    private unroll(): void {
        if (this.isHistoryState(history.state)) {
            history.go(-1);
            window.setTimeout(() => {
                this.unroll();
            }, 0)
        } else {
            this.uid = undefined;

            // inject artificial state to clear any subsequent state entries
            history.pushState(null, '');

            // make sure the artificial state is discarded when we manipulate the history again
            history.go(-1);
        }
    }

    private isHistoryState(historyState: any): historyState is HistoryState<T> {
        const state = historyState as HistoryState<T> | null | undefined;
        if (state) {
            return state.uid == this.uid;
        } else {
            return false;
        }
    }

    private pushHistoryState(item: T): void {
        const state: HistoryState<T> = {
            uid: this.uid,
            item: item,
        };
        history.pushState(state, '');
    }

    private addEventListener(): void {
        window.addEventListener('popstate', this.eventFn);
    }

    private removeEventListener(): void {
        window.removeEventListener('popstate', this.eventFn);
    }
}
