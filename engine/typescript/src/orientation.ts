declare interface RationalLike {
    numerator: number;
    denominator: number;
}

export class RationalNumber extends Number implements RationalLike {
    numerator: number;
    denominator: number;

    constructor(numerator: number, denominator: number) {
        super(numerator / denominator);
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public override toString(): string {
        return `${this.numerator}/${this.denominator}`;
    }
}

type MetadataValue = string | RationalNumber | HTMLImageElement;

function isRationalLike(value: any): value is RationalLike {
    return value !== undefined && value.numerator !== undefined && value.denominator !== undefined;
}

/**
 * Orientation constants.
 * Position names represent how row #0 and column #0 are oriented, e.g. TopLeft is the upright orientation.
 * Positive numeric constants are aligned with values in EXIF standard.
 */
export enum ImageOrientation {
    WrongImageType = -2,  // image type cannot have EXIF information; e.g. GIF and PNG do not support orientation
    NoInformation = -1,  // EXIF information is missing from file or cannot be retrieved
    Unknown = 0,
    TopLeft = 1,
    TopRight = 2,
    BottomRight = 3,
    BottomLeft = 4,
    LeftTop = 5,
    RightTop = 6,
    RightBottom = 7,
    LeftBottom = 8
}

export type ImageMetadata = {
    orientation: ImageOrientation;
    metadata?: { [key: string]: MetadataValue; };
}

declare interface EXIFService {
    getData: (image: HTMLImageElement, callback: () => void) => void;
}

declare global {
    var EXIF: EXIFService;
}

declare interface HTMLImageMetadataElement extends HTMLImageElement {
    iptcdata: { [key: string]: string };
    exifdata: { [key: string]: string };
}

/**
 * Retrieves image EXIF orientation of the camera relative to the scene.
 * @param url The image URL.
 * @param callback Invoked passing the EXIF orientation.
 */
function getImageOrientationFromURL(url: string): Promise<ImageOrientation> {
    if (!/\.jpe?g$/i.test(url)) {
        return Promise.resolve(ImageOrientation.WrongImageType);  // wrong image format, no EXIF data present in image formats GIF or PNG
    } else {
        return new Promise((resolve) => {
            let xhr = new XMLHttpRequest();
            xhr.open('get', url);
            xhr.responseType = 'blob';
            xhr.onload = () => {
                resolve(getImageOrientationFromBlob(xhr.response));
            };
            xhr.onerror = () => {
                resolve(ImageOrientation.NoInformation);
            }
            xhr.send();
        });
    }
}

/**
 * Retrieves image EXIF orientation of the camera relative to the scene.
 * @param blob The image data as a binary large object.
 * @param callback Invoked passing the EXIF orientation.
 */
function getImageOrientationFromBlob(blob: Blob): Promise<ImageOrientation> {
    return new Promise((resolve) => {
        let reader = new FileReader();
        reader.onload = () => {
            let view = new DataView(reader.result as ArrayBuffer);
            if (view.getUint16(0) != 0xFFD8) {
                return resolve(ImageOrientation.WrongImageType);  // wrong image format, not a JPEG image
            }

            let length = view.byteLength;
            let offset = 2;
            while (offset < length) {
                let marker = view.getUint16(offset);
                offset += 2;
                if (marker == 0xFFE1) {  // application marker APP1
                    // EXIF header
                    if (view.getUint32(offset += 2) != 0x45786966) {  // corresponds to string "Exif"
                        return resolve(ImageOrientation.NoInformation);  // EXIF data absent
                    }

                    // TIFF header
                    let little = view.getUint16(offset += 6) == 0x4949;  // check if "Intel" (little-endian) byte alignment is used
                    offset += view.getUint32(offset + 4, little);  // last four bytes are offset to Image file directory (IFD)

                    // IFD (Image file directory)
                    let tags = view.getUint16(offset, little);
                    offset += 2;
                    for (let i = 0; i < tags; i++) {
                        if (view.getUint16(offset + (i * 12), little) == 0x0112) {  // corresponds to IFD0 (main image) Orientation
                            let orientation = view.getUint16(offset + (i * 12) + 8, little) as ImageOrientation;
                            return resolve(orientation);
                        }
                    }
                } else if ((marker & 0xFF00) != 0xFF00) {  // not an application marker
                    break;
                } else {
                    offset += view.getUint16(offset);
                }
            }
            return resolve(ImageOrientation.NoInformation);  // application marker APP1 not found
        };
        reader.readAsArrayBuffer(blob);
    });
}

/**
* Retrieves EXIF image orientation and other metadata.
* @param image The image from which to extract information.
* @param extractMetadata Whether to attempt obtaining metadata other than image orientation.
* @return Promise fulfilled with EXIF orientation and metadata.
*/
export function getImageMetadata(image: HTMLImageElement, extractMetadata: boolean = false): Promise<ImageMetadata> {
    return new Promise((resolve) => {
        let url = image.src;
        if (/^file:/.test(url)) {
            return resolve({
                orientation: ImageOrientation.NoInformation
            });  // cross-origin requests are only supported for protocol schemes such as 'http' and 'https'
        }

        let EXIF = window.EXIF;
        if (extractMetadata && !!EXIF) {
            // use third-party plugin Exif.js to extract orientation and metadata, see <https://github.com/exif-js/exif-js>
            EXIF.getData(image, () => {
                let img = image as HTMLImageMetadataElement;
                let orientation = ImageOrientation.Unknown;
                let metadata: { [key: string]: MetadataValue } = {};
                let m: { [key: string]: MetadataValue } = Object.assign(
                    {},
                    img.iptcdata,
                    img.exifdata
                );
                if (Object.keys(m).length > 0) {
                    Object.keys(m).forEach((key) => {
                        let value = m[key];
                        if (key == 'thumbnail' && value !== undefined) {
                            let blob = value['blob'];
                            if (blob !== undefined) {
                                let image = document.createElement('img');
                                image.src = URL.createObjectURL(blob);
                                m[key] = image;
                            }
                        } else if (isRationalLike(value)) {
                            m[key] = new RationalNumber(value.numerator, value.denominator);
                        }
                    });

                    metadata = m;

                    let o = m['Orientation'] as (string|undefined);
                    if (o) {
                        orientation = (+o) as ImageOrientation;  // coerce to enumeration value (number constant)
                    }
                }
                resolve({orientation, metadata});
            });
        } else {
            // use simple built-in method to extract orientation
            getImageOrientationFromURL(url).then((orientation) => {
                resolve({orientation});
            });
        }
    });
}