export default class TimerController {
    /** System timer that tracks when the duration expires. */
    private timer?: number;

    /** True if a slideshow is currently active. */
    private active = false;

    /** Number of milliseconds to wait before the timer event is triggered. */
    private duration: number;

    /** Timer event. */
    private eventFn: () => void;

    constructor(eventFn: () => void, duration: number) {
        this.eventFn = eventFn;
        this.duration = duration;
    }

    /** Gets if the timer is active. */
    public get enabled(): boolean {
        return this.active;
    }

    /** Sets if the timer is active. */
     public set enabled(value: boolean) {
        this.active = value;
    }

    /** The slideshow will trigger the timer event (when the duration expires) if it is in active state. */
    public resume(): void {
        if (this.active) {
            this.startTimer();
        }
    }

    /** The slideshow will no longer trigger the timer event but remains in active state. */
    public suspend(): void {
        this.stopTimer();
    }

    /** The slideshow is set in active state and will trigger the timer event (when the duration expires). */
    public start(): void {
        this.active = true;
        this.startTimer();
    }

    /** The slideshow is set in inactive state and will no longer trigger the timer event. */
    public stop(): void {
        this.active = false;
        this.stopTimer();
    }

    /** Restarts the slideshow timer. */
    private startTimer(): void {
        this.stopTimer();
        if (this.duration > 0) {
            this.timer = window.setTimeout(this.eventFn, this.duration);
        }
    }

    /** Stops the slideshow timer. */
    private stopTimer(): void {
        if (this.timer) {
            window.clearTimeout(this.timer);
            this.timer = undefined;
        }
    }

}
