/**
 * boxplusx: a versatile lightweight pop-up window engine
 * @author  Levente Hunyadi
 * @version 1.0
 * @remarks Copyright (C) 2009-2021 Levente Hunyadi
 * @remarks Licensed under GNU/GPLv3, see https://www.gnu.org/licenses/gpl-3.0.html
 * @see     https://hunyadi.info.hu/projects/boxplusx
**/

import { buildQuery, isHashChange, parseURL } from './urls';
import { isFormControl } from './htmldom';
import Draggable from './draggable';
import { getImageMetadata } from './orientation';
import TimerController from './timer';
import TouchController from './touch';
import Trail from './trail';

declare interface BoxPlusXItemProperties {
    /** URL pointing to the item to display (either relative or absolute). */
    url: string;
    /** Optional placeholder image for the item. */
    image?: HTMLImageElement;
    poster?: string;
    srcset?: string;
    /** Short caption text associated with the item. May contain HTML tags. */
    title: string;
    /** Longer description text associated with the item. May contain HTML tags. */
    description: string;
    /** URL pointing to a high-resolution original to be downloaded. */
    download?: string;
}

/** Position of control with respect to the viewport area. */
enum BoxPlusXPosition {
    Hidden = 'hidden',
    Above = 'above',
    Top = 'top',
    Bottom = 'bottom',
    Below = 'below'
}

/** Text writing system. */
enum BoxPlusXWritingSystem {
    LeftToRight = 'ltr',
    RightToLeft = 'rtl'
}

/**
 * Options for the boxplusx lightbox pop-up window.
 * Unrecognized options set via the loosely-typed parameter object are silently ignored.
 */
declare interface BoxPlusXOptions {
    /** A unique identifier to assign to the pop-up window container. This helps add individual styling to dialog instances. */
    id?: string;
    /** Time spent (in ms) viewing an image when slideshow mode is active, or 0 to disable slideshow mode. */
    slideshow: number;
    /** Whether to start a slideshow when the dialog opens. */
    autostart: boolean;
    /** Whether the image/content sequence loops such that the first image/content follows the last. */
    loop: boolean;
    /** Preferred default width for the content shown in the dialog. */
    preferredWidth: number;
    /** Preferred default height for the content shown in the dialog. */
    preferredHeight: number;
    useDevicePixelRatio: boolean;
    /** Position of quick-access navigation bar w.r.t. main viewport area. */
    navigation: BoxPlusXPosition;
    /** Position of control buttons w.r.t. main viewport area. */
    controls: BoxPlusXPosition;
    /** Position of captions w.r.t. main viewport area. */
    captions: BoxPlusXPosition;
    /** Whether to permit the user to open the context menu inside the dialog. */
    contextmenu: boolean;
    /** Whether to show image metadata. (Requires third-party plugin Exif.js, see <https://github.com/exif-js/exif-js>.) */
    metadata: boolean;
    /** Whether layout should conform to a left-to-right or right-to-left writing system. */
    dir: BoxPlusXWritingSystem;
    /** Whether navigation in the pop-up window responds to the browser's "Back" and "Forward" buttons. */
    history: boolean;
}

const BoxPlusXOptionsDefaults: BoxPlusXOptions = {
    id: undefined,
    slideshow: 0,
    autostart: false,
    loop: false,
    preferredWidth: 800,
    preferredHeight: 600,
    useDevicePixelRatio: true,
    navigation: BoxPlusXPosition.Bottom,
    controls: BoxPlusXPosition.Below,
    captions: BoxPlusXPosition.Below,
    contextmenu: true,
    metadata: false,
    dir: BoxPlusXWritingSystem.LeftToRight,
    history: false
};

/** @ExportDecoratedItems */
function sealed(_: Function) {

}

declare interface Preloader {
    load: () => void;
}

declare interface HTMLPreloadableImageElement extends HTMLImageElement {
    preloader?: Preloader;
}

/** Content type shown in the pop-up window. */
enum BoxPlusXContentType {
    None,
    Unavailable,
    Image,
    Video,
    EmbeddedContent,
    DocumentFragment,
    Frame
}

/** Determine how content behaves when the container is resized. */
enum BoxPlusXDimensionBehavior {
    /** The item does not permit resizing (e.g. HTML <object> element with fixed width and height). */
    FixedSize,
    /** The item has fixed aspect ratio (e.g. HTML <video> element). */
    FixedAspectRatio,
    /** The item width and height can be set independently. */
    Resizable,
    /** The item has an intrinsic width and height but either of these may be set to a smaller value when there is insufficient space. */
    ResizableBestFit
}

function* matchAll(str: string, regExp: RegExp): Generator<RegExpExecArray, undefined, undefined> {
    let match: RegExpExecArray | null;
    while (match = regExp.exec(str)) {
        yield match;
    }
    return undefined;
}

function parseOptionalInteger(str: string | undefined, def: number): number {
    return parseInt(str ?? '', 10) || def;
}

function parseOptionalBoolean(str: string | undefined): boolean {
    return str === 'true' || !!parseInt(str ?? '', 10);
}

/**
 * Checks if a location identifies an image.
 * @param path A path or the path component of a URL.
 * @return True if the path is likely to identify an image.
 */
function isImageFile(path: string): boolean {
    return /\.(gif|jpe?g|png|svg|webp)$/i.test(path);
}

/**
 * Sets all undefined properties on an object using a reference object.
 */
function applyDefaults<T>(obj: Partial<T> | undefined, ref: T): T {
    let extended: Partial<T> = obj || {};
    for (const prop in JSON.parse(JSON.stringify(ref)) as typeof ref) {  // use JSON functions to clone object
        if (!Object.prototype.hasOwnProperty.call(extended, prop)) {
            extended[prop] = ref[prop];
        }
    }
    return extended as T;
}

/**
 * Removes all children of an HTML element.
 * @param elem The HTML element whose children to remove.
 */
function removeChildNodes(elem: Element) {
    while (elem.hasChildNodes()) {
        elem.removeChild(elem.lastChild as Node);
    }
}

/**
 * Toggles a CSS class on an element.
 * @param elem The HTML element to add the class to or remove the class from.
 * @param cls The CSS class name.
 * @param state If true, the class is added; if false, removed.
 */
function toggleClass(elem: Element, cls: string, state: boolean): void {
    elem.classList.toggle(cls, state);
}

/**
 * Sets the visibility of an HTML element.
 * @param elem The HTML element to inspect.
 * @param True if the object is to be made visible.
 */
function setVisible(elem: Element, state: boolean): void {
    toggleClass(elem, 'boxplusx-hidden', !state);
}

/**
* Determines the visibility of an HTML element.
* @param elem The HTML element to inspect.
* @return True if the object is visible.
*/
function isVisible(elem: Element): boolean {
    return !elem.classList.contains('boxplusx-hidden');
}

/**
 * Creates a HTML <div> element, acting as a building block for the dialog.
 * @param name The class name the element gets.
 * @param hidden Whether the element is initially hidden.
 * @param children Any children the element should have.
 * @return The newly created element, ready for injection into the DOM.
 */
function createElement(name: string, hidden?: boolean, children?: Element[]): HTMLDivElement {
    let elem = document.createElement('div');
    elem.classList.add('boxplusx-' + name);
    setVisible(elem, !hidden);
    if (children) {
        elem.append(...children);
    }
    return elem;
}

/** Creates several HTML <div> elements, acting as building blocks for the dialog. */
function createElements(names: string[]): HTMLDivElement[] {
    return names.map((name) => {
        return createElement(name);
    });
}

interface BoxPlusXItemText {
    /** A text suitable for a caption or the title of a browser tab or window. */
    title: string;
    /** A text suitable for a caption or description text. */
    description: string;
}

/**
 * Returns the title and description text for a content item.
 * @param elem The HTML element whose textual description to extract.
 */
function getItemText(elem: Element): BoxPlusXItemText {
    let title = '';
    let description = '';

    if (elem instanceof HTMLAnchorElement) {
        const dataset = elem.dataset;

        const dataTitle = dataset['title'];
        if (dataTitle !== undefined) {
            title = dataTitle;
        } else {
            // an HTML anchor element that nests an HTML image element with an "alt" attribute
            const image = elem.querySelector('img');
            if (image !== null) {
                const alternateText = image.getAttribute('alt');
                if (alternateText !== null) {
                    title = alternateText;
                }
            }
        }

        const dataSummary = dataset['summary'];
        if (dataSummary !== undefined) {
            description = dataSummary;
        } else {
            // an HTML anchor element with a "title" attribute
            const titleAttrText = elem.getAttribute('title');
            if (titleAttrText !== null) {
                description = titleAttrText;
            }
        }

        if (title === description) {
            description = '';
        }
    }

    return {title, description};
}

/** Generates item properties from an HTML element collection. */
function elementsToProperties(elems: HTMLElement[]): BoxPlusXItemProperties[] {
    return elems.map((elem) => {
        let {title, description} = getItemText(elem);

        let url = '';
        let poster: string | undefined;
        let srcset: string | undefined;
        if (elem instanceof HTMLAnchorElement) {
            url = elem.href;
            let dataset = elem.dataset;
            poster = dataset['poster'];
            srcset = dataset['srcset'];
        }

        // extract the HTML data attribute "download", which tells the engine where to look for the high-resolution
        // original, should the visitor choose to save a copy of the image to their computer
        let download = elem.dataset['download'];

        let image: HTMLImageElement | undefined;
        let images = elem.getElementsByTagName('img');
        if (images.length > 0) {
            image = images[0];
        }

        return {
            url,
            image,
            poster,
            srcset,
            title,
            description,
            download,
        };
    });
}

declare interface IBoxPlusXDialog {
    bind(items: NodeListOf<HTMLAnchorElement> | HTMLAnchorElement[]): (index: number) => void;
    open(members: BoxPlusXItemProperties[], index: number): void;
    show(index: number): void;
    close(): void;
    navigate(index: number): void;
    first(): void;
    previous(): void;
    next(): void;
    last(): void;
    start(): void;
    stop(): void;
    metadata(): void;
    download(): void;
}

declare interface IBoxPlusXDialogFactory {
    new(options: Partial<BoxPlusXOptions> | undefined): IBoxPlusXDialog;
    discover(strict: boolean, tag: string | undefined, options: Partial<BoxPlusXOptions> | undefined): void;
}

/**
 * The boxplusx lightbox pop-up window instance.
 * Though typically used as a singleton, the interface permits instantiating multiple instances.
 */
@sealed
export class BoxPlusXDialog implements IBoxPlusXDialog {
    /** Binds a set of elements to this dialog instance. */
    public bind(items: NodeListOf<HTMLAnchorElement> | HTMLAnchorElement[]): (index: number) => void {
        let properties = elementsToProperties(Array.from(items));
        let openfun = (index: number) => {
            this.open(properties, index);
        };
        items.forEach((elem, index) => {
            elem.addEventListener('click', (event) => {
                event.preventDefault();
                openfun(index);
            }, false);
        });
        return openfun;
    }

    private options: BoxPlusXOptions;

    private outerContainer: HTMLDivElement;
    private dialog: HTMLDivElement;
    private contentWrapper: HTMLDivElement;
    private viewport: HTMLDivElement;
    private caption: HTMLDivElement;
    private captionTitle: HTMLDivElement;
    private captionDescription: HTMLDivElement;
    private aspectHolder: HTMLDivElement;
    private innerContainer: HTMLDivElement;
    private expander: HTMLDivElement;
    private navigationArea: HTMLDivElement;
    private navigationBar: HTMLDivElement;
    private progressIndicator: HTMLDivElement;

    /** Information about elements, part of the same group, to be displayed in the pop-up window. */
    private members: BoxPlusXItemProperties[] = [];

    /** Index of current item shown. */
    private current: number = 0;

    /** History of item indexes previously seen since the pop-up window was opened. */
    private trail: Trail<number> | undefined;

    /** Slideshow controller. */
    private slideshow: TimerController;

    /** Aspect behavior for the item currently displayed. */
    private aspect = BoxPlusXDimensionBehavior.FixedAspectRatio;

    /** Preferred width for the item currently displayed. */
    private preferredWidth: number;

    /** Preferred height for the item currently displayed. */
    private preferredHeight: number;

    /** Content type currently shown in the pop-up window. */
    private contentType = BoxPlusXContentType.None;

    /** Whether content size is reduced to fit available viewport area. */
    private shrinkToFit: boolean = true;

    /** Whether content size is allowed to grow to take all available viewport area. */
    private expandToFit: boolean = false;

    /** Initializes the layout and behavior of the pop-up dialog. */
    constructor(options: Partial<BoxPlusXOptions> | undefined) {
        this.options = applyDefaults(options, BoxPlusXOptionsDefaults);

        // builds the boxplusx pop-up window HTML structure, as if by injecting the following into the DOM:
        //
        // <div class="boxplusx-container boxplusx-hidden">
        //     <div class="boxplusx-dialog">
        //         <div class="boxplusx-wrapper boxplusx-hidden">
        //             <div class="boxplusx-wrapper">
        //                 <div class="boxplusx-wrapper">
        //                     <div class="boxplusx-viewport">
        //                         <div class="boxplusx-aspect"></div>
        //                         <div class="boxplusx-content"></div>
        //                         <div class="boxplusx-expander"></div>
        //                         <div class="boxplusx-previous"></div>
        //                         <div class="boxplusx-next"></div>
        //                     </div>
        //                     <div class="boxplusx-navigation">
        //                         <div class="boxplusx-navbar">
        //                             <div class="boxplusx-navitem">
        //                                 <div class="boxplusx-aspect"></div>
        //                                 <div class="boxplusx-navimage"></div>
        //                             </div>
        //                         </div>
        //                         <div class="boxplusx-rewind"></div>
        //                         <div class="boxplusx-forward"></div>
        //                     </div>
        //                 </div>
        //                 <div class="boxplusx-controls">
        //                     <div class="boxplusx-previous"></div>
        //                     <div class="boxplusx-next"></div>
        //                     <div class="boxplusx-close"></div>
        //                     <div class="boxplusx-start"></div>
        //                     <div class="boxplusx-stop"></div>
        //                     <div class="boxplusx-download"></div>
        //                     <div class="boxplusx-metadata"></div>
        //                 </div>
        //             </div>
        //             <div class="boxplusx-caption">
        //                 <div class="boxplusx-title"></div>
        //                 <div class="boxplusx-description"></div>
        //             </div>
        //         </div>
        //         <div class="boxplusx-progress boxplusx-hidden"></div>
        //     </div>
        // </div>

        // create elements
        this.aspectHolder = createElement('aspect');
        this.innerContainer = createElement('content');
        this.expander = createElement('expander');
        this.navigationBar = createElement('navbar');
        this.navigationArea = createElement('navigation', false, [this.navigationBar].concat(createElements(['rewind', 'forward'])));
        this.viewport = createElement('viewport', false, [this.aspectHolder, this.innerContainer, this.expander].concat(createElements(['previous', 'next'])));
        let controls = createElement('controls', false, createElements(['previous', 'next', 'close', 'start', 'stop', 'download', 'metadata']));
        this.captionTitle = createElement('title');
        this.captionDescription = createElement('description');
        this.caption = createElement('caption', false, [this.captionTitle, this.captionDescription]);
        let innerWrapper = createElement('wrapper', false, [this.viewport, this.navigationArea]);
        let outerWrapper = createElement('wrapper', false, [innerWrapper, controls]);
        this.contentWrapper = createElement('wrapper', true, [outerWrapper, this.caption]);
        this.progressIndicator = createElement('progress', true);
        this.dialog = createElement('dialog', false, [this.contentWrapper, this.progressIndicator]);
        this.outerContainer = createElement('container', true, [this.dialog]);
        if (this.options.id) {
            this.outerContainer.id = this.options.id;
        }

        // arrange layout
        this.caption.classList.add('boxplusx-' + this.options.captions);
        controls.classList.add('boxplusx-' + this.options.controls);
        this.navigationArea.classList.add('boxplusx-' + this.options.navigation);

        document.body.appendChild(this.outerContainer);

        this.preferredWidth = this.options.preferredWidth;
        this.preferredHeight = this.options.preferredHeight;

        this.outerContainer.addEventListener('click', (event) => {
            if (event.target === this.outerContainer) {
                this.close();
            }
        }, false);

        this.addEventToAllElements('click', {
            'previous': () => { this.previous(); },
            'next': () => { this.next(); },
            'close': () => { this.close(); },
            'start': () => { this.start(); },
            'stop': () => { this.stop(); },
            'metadata': () => { this.metadata(); },
            'download': () => { this.download(); },
            'rewind': () => { this.stopNavigationBar(); },
            'forward': () => { this.stopNavigationBar(); }
        });
        this.addEventToAllElements('mouseover', {
            'rewind': () => { this.rewindNavigationBar(); },
            'forward': () => { this.forwardNavigationBar(); }
        });
        this.addEventToAllElements('mouseout', {
            'rewind': () => { this.stopNavigationBar(); },
            'forward': () => { this.stopNavigationBar(); }
        });

        if (!this.options.contextmenu) {
            this.dialog.addEventListener('contextmenu', (event) => {
                event.preventDefault();
            });
        }

        this.outerContainer.dir = this.options.dir;

        // set up drag and drop for content
        new Draggable(this.viewport, this.innerContainer);

        // set up slideshow controller
        this.slideshow = new TimerController(() => { this.next() }, this.options.slideshow);

        let toggleShrinkToFit = () => {
            if (this.preferredWidth > this.viewport.clientWidth || this.preferredHeight > this.viewport.clientHeight) {
                this.shrinkToFit = !this.shrinkToFit;
                const index = this.current;
                this.navigateToIndex(index);
            }
        }
        this.expander.addEventListener('click', toggleShrinkToFit);
        this.viewport.addEventListener('dblclick', toggleShrinkToFit);

        // prevent mouse wheel events from view area from propagating to document view
        this.innerContainer.addEventListener('mousewheel', (event) => {
            let wheelEvent = event as WheelEvent;
            let canScroll = window.getComputedStyle(this.innerContainer).overflowY != 'hidden';
            let maxScroll = this.innerContainer.scrollHeight - this.innerContainer.clientHeight;
            if (canScroll && maxScroll > 0) {
                let scrollTop = this.innerContainer.scrollTop;
                let deltaY = wheelEvent.deltaY;
                if ((scrollTop === maxScroll && deltaY > 0) || (scrollTop === 0 && deltaY < 0)) {
                    wheelEvent.preventDefault();
                }
            }
        });

        // map key to action
        let keyActions = new Map<number, () => void>();
        keyActions.set(27, () => { this.close(); });  // ESC
        keyActions.set(36, () => { this.first(); });  // HOME
        keyActions.set(35, () => { this.last(); });   // END
        const prevFn = () => { this.previous(); };
        const nextFn = () => { this.next(); };
        switch (this.options.dir) {
            case BoxPlusXWritingSystem.LeftToRight:
                keyActions.set(37, prevFn);  // left arrow
                keyActions.set(39, nextFn);  // right arrow
                break;
            case BoxPlusXWritingSystem.RightToLeft:
                keyActions.set(39, prevFn);  // right arrow
                keyActions.set(37, nextFn);  // left arrow
                break;
        }

        // pressing a key
        window.addEventListener('keydown', (keyboardEvent) => {
            if (isVisible(this.outerContainer)) {
                // let form elements handle their own input
                if (isFormControl(keyboardEvent.target as Element)) {
                    return;
                }

                let keyAction = keyActions.get(keyboardEvent.which || keyboardEvent.keyCode)
                if (keyAction) {
                    keyAction();
                    keyboardEvent.preventDefault();
                }
            }
        }, false);

        // navigation by swipe
        new TouchController(this.viewport, {
            right: () => {
                if (this.shrinkToFit) {
                    this.previous();
                }
            },
            left: () => {
                if (this.shrinkToFit) {
                    this.next();
                }
            }
        });

        // mobile-friendly forward and rewind for quick-access navigation bar
        new TouchController(this.navigationBar, {
            start: () => {
                this.stopNavigationBar();
            },
            right: () => {
                this.rewindNavigationBar();
            },
            left: () => {
                this.forwardNavigationBar();
            }
        });

        // window resize
        window.addEventListener('resize', (_: Event) => {
            if (isVisible(this.outerContainer)) {
                this.setMaximumDialogSize();
                this.repositionNavigationBar();
                this.updateExpanderState();
            }
        });

        if (this.options.history) {
            this.trail = new Trail((index: number | undefined) => {
                if (index !== undefined) {
                    this.navigateToIndex(index);
                } else {
                    this.close();
                }
            });
        }
    }

    /** Opens the pop-up window. */
    public open(members: BoxPlusXItemProperties[], index: number): void {
        this.members = members;

        // populate quick-access navigation bar
        const isNavigationVisible = members.length > 1 && this.options.navigation != BoxPlusXPosition.Hidden;
        setVisible(this.navigationArea, isNavigationVisible);
        if (isNavigationVisible) {
            members.forEach((member, i) => {
                let navigationAspect = createElement('aspect');
                let navigationImage = createElement('navimage');
                let navigationItem = createElement('navitem', false, [navigationAspect, navigationImage]);
                let allowAction = true;
                navigationItem.addEventListener('touchstart', () => {
                    if (this.isNavigationBarSliding()) {
                        allowAction = false;
                    }
                });
                navigationItem.addEventListener('click', () => {
                    if (allowAction) {
                        this.navigate(i);
                    }
                    allowAction = true;
                });

                let image = member.image;
                if (image) {
                    let img = image;
                    let setNavigationImage = () => {
                        let aspectStyle = navigationAspect.style;
                        if (img.naturalWidth && img.naturalHeight) {
                            aspectStyle.setProperty('width', img.naturalWidth + 'px');
                            aspectStyle.setProperty('padding-top', (100.0 * img.naturalHeight / img.naturalWidth) + '%');
                        }
                        if (img.src) {
                            navigationImage.style.setProperty('background-image', 'url("' + img.src + '")');
                        }
                    };
                    if (img.src && img.complete) {  // make sure the image is available
                        setNavigationImage();
                    } else {
                        // set aspect properties immediately when the image is loaded
                        img.addEventListener('load', setNavigationImage);

                        let preloadableImage = image as HTMLPreloadableImageElement;

                        // trigger pre-loader service if registered by another script
                        if (preloadableImage.preloader) {
                            preloadableImage.preloader.load();
                        }
                    }
                }

                navigationImage.innerText = (i + 1) + '';
                this.navigationBar.appendChild(navigationItem);
            });
        }

        this.show(index);
    }

    /** Show the pop-up window. */
    public show(index: number): void {
        if (this.options.autostart && this.options.slideshow > 0) {
            this.slideshow.enabled = true;
        }

        setVisible(this.outerContainer, true);
        setVisible(this.progressIndicator, true);

        this.navigateToIndex(index);
    }

    public close(): void {
        this.slideshow.stop();

        // clear history track
        this.current = 0;
        if (this.trail) {
            this.trail.clear();
        }

        // call private method that does not manipulate history
        this.hideWindow();
    }

    public navigate(index: number): void {
        if (index != this.current) {
            this.navigateToIndex(index);
        }
    }

    public first(): void {
        this.navigate(0);
    }

    public previous(): void {
        const index = this.current;
        if (index > 0) {
            this.navigate(index - 1);
        } else if (this.options.loop) {
            this.last();
        }
    }

    public next(): void {
        const index = this.current;
        if (index < this.members.length - 1) {
            this.navigate(index + 1);
        } else if (this.options.loop) {
            this.first();
        }
    }

    public last(): void {
        this.navigate(this.members.length - 1);
    }

    public start(): void {
        this.slideshow.start();
        if (this.options.slideshow > 0) {
            this.updateControls();
        }
    }

    public stop(): void {
        this.slideshow.stop();
        if (this.options.slideshow > 0) {
            this.updateControls();
        }
    }

    public metadata(): void {
        let metadata = this.queryElement('detail');
        if (metadata) {
            setVisible(metadata, !isVisible(metadata));
        }
    }

    public download(): void {
        const index = this.current;
        const url = this.members[index]!.download;
        if (url !== undefined) {
            let anchor = document.createElement('a');
            anchor.href = url;
            document.body.appendChild(anchor);
            anchor.click();
            document.body.removeChild(anchor);
        }
    };

    private queryElement(identifier: string): HTMLElement | null {
        return this.dialog.querySelector('.boxplusx-' + identifier);
    }

    private queryAllElements(identifier: string): NodeListOf<HTMLElement> {
        return this.dialog.querySelectorAll('.boxplusx-' + identifier);
    }

    private applyAllElements(identifier: string, func: (elem: Element) => void) {
        this.queryAllElements(identifier).forEach((elem) => {
            func(elem);
        });
    }

    private addEventToAllElements(eventName: string, map: { [identifier: string]: () => void }) {
        for (const identifier of Object.getOwnPropertyNames(map)) {
            const eventFn = map[identifier]!;
            this.applyAllElements(identifier, (elem: Element) => {
                elem.addEventListener(eventName, eventFn, false);
            });
        }
    }

    private isContentInteractive(type: BoxPlusXContentType): boolean {
        switch (type) {
            case BoxPlusXContentType.Unavailable:
            case BoxPlusXContentType.Image:
                return false;
        }
        return true;
    }

    /** Sets a content type that helps identify what is shown in the pop-up window viewport area. */
    private setContentType(contentType: BoxPlusXContentType): void {
        function getContentTypeString(type: BoxPlusXContentType): string {
            switch (type) {
                case BoxPlusXContentType.Unavailable:
                    return 'unavailable';
                case BoxPlusXContentType.Image:
                    return 'image';
                case BoxPlusXContentType.Video:
                    return 'video';
                case BoxPlusXContentType.EmbeddedContent:
                    return 'embed';
                case BoxPlusXContentType.DocumentFragment:
                    return 'document';
                case BoxPlusXContentType.Frame:
                    return 'frame';
                case BoxPlusXContentType.None:
                    return 'none';
            }
        }

        let classList = this.innerContainer.classList;
        classList.remove('boxplusx-' + getContentTypeString(this.contentType));
        classList.remove('boxplusx-interactive');
        this.contentType = contentType;
        classList.add('boxplusx-' + getContentTypeString(contentType));
        if (this.isContentInteractive(contentType)) {
            classList.add('boxplusx-interactive');
        }
    }

    private updateControls(): void {
        let index = this.current;
        let isFirstItem = index == 0;
        let members = this.members;
        let isLastItem = index >= members.length - 1;
        let loop = this.options.loop && !(isFirstItem && isLastItem);
        let slideshow = this.options.slideshow > 0;

        this.applyAllElements('previous', (elem) => {
            setVisible(elem, loop || !isFirstItem);
        });
        this.applyAllElements('next', (elem) => {
            setVisible(elem, loop || !isLastItem);
        });
        this.applyAllElements('start', (elem) => {
            setVisible(elem, slideshow && !this.slideshow.enabled && !isLastItem);
        });
        this.applyAllElements('stop', (elem) => {
            setVisible(elem, slideshow && this.slideshow.enabled);
        });
        this.applyAllElements('download', (elem) => {
            setVisible(elem, members[index]!.download !== undefined);
        });
        this.applyAllElements('metadata', (elem) => {
            setVisible(elem, this.options.metadata && !!this.queryElement('detail'));
        });
    }

    private updateExpanderState(): void {
        let isOversize = this.preferredWidth > this.viewport.clientWidth || this.preferredHeight > this.viewport.clientHeight;
        setVisible(this.expander, isOversize && !this.isContentInteractive(this.contentType));
        toggleClass(this.expander, 'boxplusx-collapse', !this.shrinkToFit);
        toggleClass(this.expander, 'boxplusx-expand', this.shrinkToFit);
    }

    private hideWindow(): void {
        // reset shrink to fit
        this.shrinkToFit = true;
        this.expandToFit = false;
        this.updateExpanderState();

        // reset content displayed
        this.removeAnimationProperties();
        this.clearContent();
        this.setContentType(BoxPlusXContentType.None);
        removeChildNodes(this.navigationBar);
        setVisible(this.contentWrapper, false);
        setVisible(this.outerContainer, false);  // must come before manipulating history
    }

    /** Reveals the content to be displayed. */
    private showContent(): void {
        this.removeAnimationProperties();
        setVisible(this.progressIndicator, false);

        let index = this.current;
        if (index >= this.members.length - 1) {
            this.slideshow.stop();
        }
        this.updateControls();

        setVisible(this.contentWrapper, true);

        // dialog must be visible to have valid offset values
        this.repositionNavigationBar();
        this.updateExpanderState();

        this.slideshow.resume();
    }

    /**
     * Trigger dialog animation to morph into a size suitable for the next item.
     * @param aspect Specifies how the dialog should respond when resized.
     * @param originalWidth The original dialog CSS width to start with.
     * @param originalHeight The original dialog CSS height to start with.
     */
    private morphDialog(aspect: BoxPlusXDimensionBehavior, originalWidth?: string, originalHeight?: string) {
        this.aspect = aspect;

        // save current dialog dimensions and aspect ratio
        let computedStyle = window.getComputedStyle(this.dialog);
        const currentWidth = originalWidth || computedStyle.getPropertyValue('width');
        const currentHeight = originalHeight || computedStyle.getPropertyValue('height');
        this.removeAnimationProperties();

        // use temporarily exposed elements for calculations
        setVisible(this.contentWrapper, true);

        let viewportClassList = this.viewport.classList;
        viewportClassList.remove('boxplusx-fixedaspect');
        viewportClassList.remove('boxplusx-draggable');
        if (BoxPlusXDimensionBehavior.FixedSize === aspect || BoxPlusXDimensionBehavior.FixedAspectRatio === aspect) {
            // set new aspect ratio
            // if specified as a percentage, CSS padding is expressed in terms of container width (even for top
            // and bottom padding), which we utilize here to make item grow/shrink vertically as it grows/shrinks
            // horizontally
            let aspectStyle = this.aspectHolder.style;
            if (this.expandToFit) {
                aspectStyle.setProperty('width', '100vw');
            } else {
                aspectStyle.setProperty('width', this.preferredWidth + 'px');
            }
            aspectStyle.setProperty('padding-top', (100.0 * this.preferredHeight / this.preferredWidth) + '%');
            viewportClassList.add('boxplusx-fixedaspect');
        } else if (BoxPlusXDimensionBehavior.ResizableBestFit === aspect) {
            viewportClassList.add('boxplusx-draggable');
        } else if (BoxPlusXDimensionBehavior.Resizable === aspect) {
            let containerStyle = this.innerContainer.style;
            containerStyle.setProperty('width', this.preferredWidth + 'px');
            containerStyle.setProperty('max-height', this.preferredHeight + 'px');
        }

        this.setMaximumDialogSize();

        // get desired target size with all inner controls temporarily visible
        const desiredWidth = computedStyle.getPropertyValue('width');
        const desiredHeight = computedStyle.getPropertyValue('height');
        const desiredMaxWidth = computedStyle.getPropertyValue('max-width');

        // animation transition end function
        let appliedStyle = this.dialog.style;
        let fn = () => {
            if (isVisible(this.outerContainer)) {
                appliedStyle.setProperty('max-width', desiredMaxWidth);
                this.showContent();
            }
        }

        if (currentWidth != desiredWidth || currentHeight != desiredHeight) {  // dialog animation required to fit new content size
            // hide elements after calculations have been made
            setVisible(this.contentWrapper, false);

            // reset previous dialog dimensions
            appliedStyle.removeProperty('max-width');
            appliedStyle.setProperty('width', currentWidth);
            appliedStyle.setProperty('height', currentHeight);

            this.dialog.classList.add('boxplusx-animation');

            // determine when event "transitionend" would be fired
            // helps thwart deadlock when event "transitionend" is never fired due to race condition
            const duration = Math.max.apply(null, computedStyle.getPropertyValue('transition-duration').split(',').map(function (item) {
                let value = parseFloat(item);
                if (/\ds$/.test(item)) {
                    return 1000 * value;
                } else {
                    return value;
                }
            }));
            window.setTimeout(fn, duration);
        } else {  // no dialog animation required, only swap content
            fn();
        }

        // start CSS transition by setting desired size for pop-up window as transition target
        appliedStyle.setProperty('width', desiredWidth);
        appliedStyle.setProperty('height', desiredHeight);
    }

    /**
     * Removes all element properties associated with dialog animation.
     */
    private removeAnimationProperties(): void {
        this.dialog.classList.remove('boxplusx-animation');

        // remove any explicit sizes applied for the sake of the CSS transition animation
        let appliedStyle = this.dialog.style;
        appliedStyle.removeProperty('width');
        appliedStyle.removeProperty('height');
    }

    /**
     * Uses the bisection algorithm to determine the dialog size.
     * @param a Lower bound (percentage) value at which the dialog fits.
     * @param b Upper bound (percentage) value at which the dialog does not fit.
     * @param applyFun Applies a value (e.g. sets content width or height).
     * @return The (percentage) value at which the dialog fits exactly.
     */
    private bisectionSearch(a: number, b: number, applyFun: (n: number) => void): number {
        /**
         * Evaluates the dialog height at a particular value.
         * @param value A parameter value to apply.
         * @return The dialog height in pixels (including border and padding) when the value is applied.
         */
        let evaluateFun = (value: number): number => {
            applyFun(value);
            return this.dialog.offsetHeight;
        }

        const containerHeight = this.outerContainer.clientHeight;

        let dlgHeightB = evaluateFun(b);  // no extra horizontal constraints
        if (dlgHeightB <= containerHeight) {
            return b;  // nothing to do; pop-up window fits vertically
        }

        let dlgHeightA = evaluateFun(a);  // force dialog take its minimum size
        if (dlgHeightA >= containerHeight) {
            applyFun(b);  // reset constraints
            return b;  // nothing to do; pop-up window too large to fit even with most constraints
        }

        // use bisection method to find least restrictive horizontal constraint that still allows the pop-up window
        // to fit vertically
        for (let n = 1; n < 10; ++n) {  // use a maximum iteration count to avoid problems with slow convergence
            let c = ((a + b) / 2) | 0;  // cast to integer for improved performance
            let dlgHeightC = evaluateFun(c);

            if (dlgHeightC < containerHeight) {
                a = c;  // found a better lower bound
                dlgHeightA = dlgHeightC;
            } else {
                b = c;  // found a better upper bound
                dlgHeightB = dlgHeightC;
            }
        }

        // when the algorithm terminates, lower and upper bound are close; apply the lower bound as the value we seek
        applyFun(a);
        return a;
    }

    /**
     * Set maximum width for dialog so that it does not exceed viewport dimensions.
     * CSS property max-height: 100% is not respected by browsers in this context: the height of the containing
     * block is not specified explicitly (i.e., it depends on content height), and the element is not absolutely
     * positioned, therefore the percentage value is treated as none (to avoid infinite re-calculation loops in
     * layout); as a work-around, we set an upper limit on width instead.
     */
    private setMaximumDialogSize(): void {
        if (BoxPlusXDimensionBehavior.FixedAspectRatio === this.aspect) {
            // for fixed aspect ratio, we vary the maximum dialog width in terms of the width of the container element
            // (browser viewport), expressed as a percentage value
            let dialogStyle = this.dialog.style;
            this.bisectionSearch(0, 1000, function (value) {
                dialogStyle.setProperty('max-width', (value / 10) + '%');
            })
        } else if (BoxPlusXDimensionBehavior.ResizableBestFit === this.aspect || BoxPlusXDimensionBehavior.Resizable === this.aspect) {
            // for dynamic aspect ratio, we vary the content holder element pixel height
            let containerStyle = this.innerContainer.style;
            containerStyle.removeProperty('max-height');
            let value = this.bisectionSearch(0, window.innerHeight, function (value) {
                containerStyle.setProperty('height', value + 'px');
            })
            containerStyle.removeProperty('height');
            containerStyle.setProperty('max-height', Math.min(value, this.preferredHeight) + 'px');
        }
    }

    /**
     * Makes the specified item currently active.
     * @param index The zero-based index of the item to be displayed.
     */
    private navigateToIndex(index: number): void {
        const member = this.members[index]!;
        this.current = index;
        if (this.trail) {
            this.trail.push(index);
        }

        let computedStyle = window.getComputedStyle(this.dialog);
        const currentWidth = computedStyle.getPropertyValue('width');
        const currentHeight = computedStyle.getPropertyValue('height');

        this.slideshow.suspend();
        setVisible(this.progressIndicator, true);

        // save caption text
        let title = member.title;
        let description = member.description;

        const href = member.url;
        const urlparts = parseURL(href);
        const path = urlparts.pathname;
        const parameters: { [key: string]: string } = Object.assign({}, urlparts.queryparams, urlparts.fragmentparams);

        this.preferredWidth = parseOptionalInteger(parameters['width'], this.options.preferredWidth);
        this.preferredHeight = parseOptionalInteger(parameters['height'], this.options.preferredHeight);
        this.expandToFit = parseOptionalBoolean(parameters['fullscreen']);

        if (isHashChange(href)) {
            const target = urlparts.id ? urlparts.id : (parameters['target'] ?? '');
            if (target) {
                let elem = document.getElementById(target);
                if (elem) {
                    let content = elem.cloneNode(true) as typeof elem;
                    this.replaceContent(content, title, description);
                    this.setContentType(BoxPlusXContentType.DocumentFragment);
                    this.morphDialog(BoxPlusXDimensionBehavior.Resizable, currentWidth, currentHeight);
                } else {
                    this.displayUnavailable();
                }
            } else {
                this.displayUnavailable();
            }
        } else if (isImageFile(path)) {
            // download image in the background
            let image = document.createElement('img');
            let srcset = member['srcset'];
            if (srcset) {
                image.srcset = srcset;
            }
            image.addEventListener('load', (_: Event) => {
                // try extracting image EXIF orientation for photos
                getImageMetadata(image, this.options.metadata).then(data => {
                    let container = document.createDocumentFragment();

                    // set image
                    let rotationContainer = document.createElement('div');
                    let imageElement = document.createElement('div');

                    if (data.orientation > 0) {
                        imageElement.classList.add('boxplusx-orientation-' + data.orientation);
                    }

                    let imageElementStyle = imageElement.style;
                    imageElementStyle.setProperty('background-image', 'url("' + image.src + '")');
                    if (image.srcset) {
                        let matches = matchAll(image.srcset, /\b(\S+)\s+([\d.]+x)\b/g);
                        let imageset = Array.from(matches, (match: RegExpMatchArray) => {
                            return 'url("' + match[1] + '") ' + match[2];
                        }).join();
                        imageElementStyle.setProperty('background-image', '-webkit-image-set(' + imageset + ')');
                    }

                    let dpr = this.options.useDevicePixelRatio ? (window.devicePixelRatio || 1) : 1;
                    let h = Math.floor(image.naturalHeight / dpr);
                    let w = Math.floor(image.naturalWidth / dpr);
                    if (!CSS.supports("image-orientation", "from-image") && data.orientation >= 5 && data.orientation <= 8) {  // image rotated by 90 or 270 degrees
                        this.preferredWidth = h;
                        this.preferredHeight = w;

                        // CSS transform does not affect bounding box for layout, enlarge/shrink CSS width/height
                        // to accommodate for transformation results
                        imageElementStyle.setProperty('width', (100 * w / h) + '%');
                        imageElementStyle.setProperty('height', (100 * h / w) + '%');
                    } else {  // image rotated by 0 or 180 degrees
                        this.preferredWidth = w;
                        this.preferredHeight = h;

                        // necessary when we re-use existing container accommodating previous image
                        imageElementStyle.removeProperty('width');
                        imageElementStyle.removeProperty('height');
                    }

                    if (!this.shrinkToFit) {
                        let rotationContainerStyle = rotationContainer.style;
                        rotationContainerStyle.setProperty('width', this.preferredWidth + 'px');
                        rotationContainerStyle.setProperty('height', this.preferredHeight + 'px');
                    }

                    rotationContainer.appendChild(imageElement);
                    container.appendChild(rotationContainer);

                    // get image metadata information
                    if (data.metadata !== undefined) {
                        let metadata = data.metadata;
                        let textElement = createElement('detail', true);
                        let table = document.createElement('table');
                        let keys = Object.keys(metadata);
                        keys.sort();
                        keys.forEach((key) => {
                            let value = metadata[key];
                            if (value !== undefined) {
                                let row = document.createElement('tr');
                                let headerCell = document.createElement('td');
                                headerCell.innerText = key;
                                let valueCell = document.createElement('td');
                                if (value instanceof HTMLElement) {
                                    valueCell.append(value);
                                } else {
                                    valueCell.innerText = value.toString();
                                }
                                row.append(headerCell, valueCell);
                                table.appendChild(row);
                            }
                        });
                        textElement.appendChild(table);
                        container.appendChild(textElement);
                    }

                    this.replaceContent(container, title, description);
                    this.caption.style.setProperty('max-width', this.preferredWidth + 'px');  // must come after replacing content to have any effect
                    this.setContentType(BoxPlusXContentType.Image);

                    // start dialog animation
                    this.morphDialog(this.shrinkToFit ? BoxPlusXDimensionBehavior.FixedAspectRatio : BoxPlusXDimensionBehavior.ResizableBestFit, currentWidth, currentHeight);
                });
            }, false);
            image.addEventListener('error', () => { this.displayUnavailable(); }, false);
            image.src = href;

            // pre-fetch next image (unless last is shown) to speed up slideshows and viewing images one after the other
            if (index < this.members.length - 1) {
                const nextmember = this.members[index + 1]!;
                const nexthref = nextmember.url;
                const nexturlparts = parseURL(nexthref);
                if (isImageFile(nexturlparts.pathname)) {
                    let nextimage = document.createElement('img');
                    nextimage.src = nexthref;
                }
            }
        } else if (/\.(mov|mpe?g|mp4|ogg|webm)$/i.test(path)) {  // supported by HTML5-native <video> tag
            let video = document.createElement('video');
            let play = createElement('play');
            let container = createElement('video', false, [video, play]);
            video.addEventListener('loadedmetadata', (_: Event) => {
                // set video
                this.replaceContent(container, title, description);
                this.setContentType(BoxPlusXContentType.Video);

                this.preferredWidth = video.videoWidth;
                this.preferredHeight = video.videoHeight;
                this.morphDialog(BoxPlusXDimensionBehavior.FixedAspectRatio, currentWidth, currentHeight);
            }, false);
            video.addEventListener('error', () => this.displayUnavailable(), false);
            video.src = href;
            let poster = member.poster;
            if (poster) {
                video.poster = poster;
            }
            play.addEventListener('click', function () {
                setVisible(play, false);
                video.controls = true;
                video.play();
            });
        } else if (/\.pdf$/.test(path)) {
            let embed = document.createElement('embed');
            embed.src = href;
            embed.type = 'application/pdf';

            this.replaceContent(embed, title, description);
            this.setContentType(BoxPlusXContentType.EmbeddedContent);
            this.morphDialog(BoxPlusXDimensionBehavior.FixedAspectRatio, currentWidth, currentHeight);
        } else {
            // check for YouTube URLs
            let match = /^https?:\/\/(?:www\.)youtu(?:\.be|be\.com)\/(?:embed\/|watch\?v=|v\/|)([-_0-9A-Z]{11,})/i.exec(href);
            if (match !== null) {
                this.displayFrame(
                    'https://www.youtube.com/embed/' + match[1] + '?' + buildQuery({ rel: '0', controls: '1', showinfo: '0' }),
                    title, description
                );
                return;
            }

            // URL to unrecognized target (a plain URL to an external location)
            this.displayFrame(href, title, description);
        }
    }

    /**
     * Clears the content in the inner container.
     * This function clears all CSS properties set from script so they revert to their values specified
     * in the stylesheet file.
     */
    private clearContent(): void {
        // remove all HTML child elements
        removeChildNodes(this.innerContainer);

        let dialogStyle = this.dialog.style;
        let aspectStyle = this.aspectHolder.style;
        let containerStyle = this.innerContainer.style;

        // remove CSS properties that force the aspect ratio
        aspectStyle.removeProperty('padding-top');
        aspectStyle.removeProperty('width');

        // remove content and content styling
        containerStyle.removeProperty('width');  // preferred width

        // remove fit to window constraints
        dialogStyle.removeProperty('max-width');
        containerStyle.removeProperty('max-height');

        this.captionTitle.innerHTML = "";
        this.captionDescription.innerHTML = "";
    }

    /**
     * Replaces the content currently displayed in the pop-up window.
     * @param content HTML content to place in the viewport area.
     * @param title The caption text title to associate with the item.
     * @param description The caption text description to associate with the item.
     */
    private replaceContent(content: DocumentFragment | Element, title: string, description: string): void {
        this.clearContent();

        this.innerContainer.appendChild(content);
        this.caption.style.removeProperty('max-width');  // reset caption style
        this.captionTitle.innerHTML = title;
        this.captionDescription.innerHTML = description;
    }

    /** Displays an indicator that the requested content is not available. */
    private displayUnavailable(): void {
        this.clearContent();

        // set unavailable image
        this.setContentType(BoxPlusXContentType.Unavailable);

        // start dialog animation
        this.morphDialog(BoxPlusXDimensionBehavior.FixedAspectRatio);
    }

    /**
     * Displays the contents of an external page in the pop-up window.
     * @param src The URL to the source to be displayed.
     * @param title The caption text title to associate with the item.
     * @param description The caption text description to associate with the item.
     */
    private displayFrame(src: string, title: string, description: string): void {
        let frame = document.createElement('iframe');
        frame.width = '' + this.preferredWidth;
        frame.height = '' + this.preferredHeight;
        frame.allow = 'fullscreen';
        frame.src = src;

        // HTML iframe must be added to the DOM in order for the 'load' event to be triggered
        this.replaceContent(frame, title, description);

        // must register 'load' event after adding to the DOM to avoid the event being triggered for blank document
        let hasFired = false;
        frame.addEventListener('load', (_: Event) => {
            // make sure spurious 'load' events are ignored
            // (the third parameter to addEventListener called 'options' is not supported in all browsers)
            if (hasFired) {
                return;
            }
            hasFired = true;

            this.setContentType(BoxPlusXContentType.Frame);
            this.morphDialog(BoxPlusXDimensionBehavior.FixedAspectRatio);
        }, false);
    }

    /** Returns the current offset of an element from the edge, taking into account text directionality. */
    private getItemEdgeOffset(item: HTMLElement): number {
        switch (this.options.dir) {
            case BoxPlusXWritingSystem.RightToLeft:
                const parentItem = item.offsetParent as HTMLElement;
                return parentItem.offsetWidth - item.offsetWidth - item.offsetLeft;  // an implementation of function offsetRight
            case BoxPlusXWritingSystem.LeftToRight:
                return item.offsetLeft;
        }
    }

    /**
     * Returns the maximum value for positioning the quick-access navigation bar.
     * Values in the range [-maximum; 0] are permitted as pixel length values for the CSS left property in order for
     * the navigation bar to remain in view.
     */
    private getNavigationRange(): number {
        return Math.max(this.navigationBar.offsetWidth - this.navigationArea.offsetWidth, 0);
    }

    /** Returns the current navigation bar position, taking into account text directionality. */
    private getNavigationPosition(): number {
        // negate computed value because the property offsetLeft or offsetRight takes values in the range [-maximum; 0]
        return -this.getItemEdgeOffset(this.navigationBar);
    }

    /**
     * Starts moving the navigation bar towards the specified target position.
     * @param targetPosition A nonnegative number, indicating target position.
     * @param duration A nonnegative number, indicating number of milliseconds for the animation to take.
     */
    private slideNavigationBar(targetPosition: number, duration: number): void {
        const rtl = this.options.dir == BoxPlusXWritingSystem.RightToLeft;
        let navigationStyle = this.navigationBar.style;
        navigationStyle.setProperty(rtl ? 'right' : 'left', (-targetPosition) + 'px');
        navigationStyle.setProperty('transition-duration', duration > 0 ? (5 * duration) + 'ms' : '');
    }

    private isNavigationBarSliding(): boolean {
        return !!this.navigationBar.style.getPropertyValue('transition-duration');
    }

    /** Re-position the navigation bar so that the active item is aligned with the left edge of the navigation area. */
    private repositionNavigationBar(): void {
        if (isVisible(this.navigationArea)) {
            // remove focus from navigation item corresponding to previously active item
            for (let k = 0; k < this.navigationBar.childNodes.length; ++k) {
                (this.navigationBar.childNodes[k] as HTMLElement).classList.remove('boxplusx-current');
            }

            // set focus on navigation item corresponding to currently active item
            const index = this.current;
            const maximum = this.getNavigationRange();  // the maximum permitted offset
            let item = this.navigationBar.childNodes[index] as HTMLElement;
            item.classList.add('boxplusx-current');

            // get the current scroll offset, which may possibly be out of view
            let scrollPosition = this.getNavigationPosition();
            const itemEdgeOffset = this.getItemEdgeOffset(item);

            // the last position to scroll forward to before the current item goes (partially) out of view
            let lastForwardScrollFit = Math.min(maximum, itemEdgeOffset);
            if (scrollPosition > lastForwardScrollFit) {
                scrollPosition = lastForwardScrollFit;
            }

            // the last position to scroll backward to before the current item goes (partially) out of view
            // subtract item width because items are left offset-aligned
            let lastBackwardScrollFit = Math.max(0, itemEdgeOffset - this.navigationArea.offsetWidth + item.offsetWidth);
            if (scrollPosition < lastBackwardScrollFit) {
                scrollPosition = lastBackwardScrollFit;
            }

            this.slideNavigationBar(scrollPosition, 0);  // temporarily disable any transition animation
        }
    }

    private rewindNavigationBar(): void {
        const maximum = this.getNavigationRange();
        const current = maximum - this.getNavigationPosition();

        // set target position for navigation bar, reached via CSS transition animation
        // furthermost position for rewinding corresponds to the navigation bar pushed to the rightmost permitted
        // position (left offset value 0), set transition duration depending on how far we are from the furthermost
        // position to get a constant movement speed, regardless of what the current navigation bar position is
        this.slideNavigationBar(0, maximum - current);
    }

    private forwardNavigationBar(): void {
        const maximum = this.getNavigationRange();
        const current = this.getNavigationPosition();

        // set target position for navigation bar, reached via CSS transition animation
        // furthermost position for forwarding corresponds to the navigation bar pushed to the leftmost permitted
        // position (greatest absolute value), set transition duration depending on how far we are from the furthermost
        // position to get a constant movement speed, regardless of what the current navigation bar position is
        this.slideNavigationBar(maximum, maximum - current);
    }

    private stopNavigationBar(): void {
        // stop CSS transition animation by forcing the current offset values returned by computed style
        this.slideNavigationBar(this.getNavigationPosition(), 0);  // temporarily disable any transition animation
    }

    /**
     * Discovers boxplusx links on a web page.
     * boxplusx links are regular HTML <a> elements whose 'rel' attribute has a value with the pattern 'boxplusx-NNN'
     * where NNN is a unique name. All items that share the same unique name are organized into the same gallery. When
     * the user clicks an item that is part of a gallery, the item opens in the pop-up window and users can navigate
     * between this and other items in the gallery without closing the pop-up window.
     */
    public static discover(strict: boolean, tag: string | undefined, options: Partial<BoxPlusXOptions> | undefined): void {
        let activator = tag ?? 'boxplusx';
        let dialog = new BoxPlusXDialog(options);

        // discover groups of pop-up window display items on a web page
        // links with "rel" attribute that start with (but are not identical to) the activation string
        const items: NodeListOf<HTMLAnchorElement> = document.querySelectorAll('a[href][rel^=' + activator + ']:not([rel=' + activator + '])');

        // make groups by name
        const groups = new Map<string, HTMLAnchorElement[]>();
        items.forEach((item: HTMLAnchorElement) => {
            let identifier = item.getAttribute('rel');

            if (!identifier) {
                return;
            }

            let group = groups.get(identifier);
            if (group === undefined) {
                group = [];
                groups.set(identifier, group);
            }

            group.push(item);
        });

        groups.forEach((group) => {
            dialog.bind(group);
        });

        [].filter.call(document.querySelectorAll('a[href][rel=' + activator + ']'), (item: HTMLAnchorElement) => {
            dialog.bind([item]);
        });

        if (!strict) {
            // individual links to images or video not part of a gallery
            let items: NodeListOf<HTMLAnchorElement> = document.querySelectorAll('a[href]:not([rel^=' + activator + '])');
            [].filter.call(items, (item: HTMLAnchorElement) => {
                return /\.(gif|jpe?g|mov|mpe?g|ogg|png|svg|web[mp])$/i.test(item.pathname) && !item.target;
            }).forEach((item: HTMLAnchorElement) => {
                dialog.bind([item]);
            });
        }
    };
}

// ensure symbol is exported by Closure Compiler
window['BoxPlusXDialog'] = BoxPlusXDialog;
