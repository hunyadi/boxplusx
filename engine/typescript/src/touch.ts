interface TouchActions {
    start?: () => void;
    left: () => void;
    right: () => void;
}

export default class TimerController {
    private actions;
    private touchStartX: number = 0;
    private lastTouch: number = 0;

    constructor(elem: HTMLElement, actions: TouchActions) {
        this.actions = actions;
        elem.addEventListener('touchstart', (touchEvent: TouchEvent) => {
            this.touchStartX = touchEvent.changedTouches[0]!.pageX;
            if (this.actions.start) {
                this.actions.start();
            }
        });
        elem.addEventListener('touchend', (touchEvent: TouchEvent) => {
            let now = new Date().getTime();
            let delta = now - this.lastTouch;
            if (delta > 0 && delta < 500) {  // double tap (two successive taps one shortly after the other)
                touchEvent.preventDefault();
            } else {  // single tap
                let x = touchEvent.changedTouches[0]!.pageX;
                if (x - this.touchStartX >= 50) {  // swipe to the right
                    this.actions.right();
                } else if (this.touchStartX - x >= 50) {  // swipe to the left
                    this.actions.left();
                }
            }
            this.lastTouch = now;
        });
    }
}
