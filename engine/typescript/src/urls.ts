/**
 * Parses a query string into name/value pairs.
 * @param querystring A string of "name=value" pairs, separated by "&".
 * @return An object where keys are parameter names, and values are parameter values.
 */
function fromQueryString(querystring: string): { [key: string]: string } {
    let parameters: { [key: string]: string } = {};
    if (querystring.length > 1) {
        querystring.substr(1).split('&').forEach((keyvalue) => {
            let index = keyvalue.indexOf('=');
            let key = index >= 0 ? keyvalue.substr(0, index) : keyvalue;
            let value = index >= 0 ? keyvalue.substr(index + 1) : '';
            parameters[decodeURIComponent(key)] = decodeURIComponent(value);
        });
    }
    return parameters;
}

export type URLComponents = {
    protocol: string,
    host: string,
    hostname: string,
    port: string,
    pathname: string,
    search: string,
    queryparams: { [key: string]: string },
    hash: string,
    id: string,
    fragmentparams: { [key: string]: string }
}

/**
* Parses a URL string into URL components.
* @param url A URL string.
* @return URL components.
*/
export function parseURL(url: string): URLComponents {
    let parser = document.createElement("a");
    parser.href = url;
    const hashBangIndex = parser.hash.indexOf('!');

    return {
        protocol: parser.protocol,
        host: parser.host,
        hostname: parser.hostname,
        port: parser.port,
        pathname: parser.pathname,
        search: parser.search,  // starts with & unless empty
        queryparams: fromQueryString(parser.search),
        hash: parser.hash,  // starts with # unless empty
        id: parser.hash.substr(1, (hashBangIndex >= 0 ? hashBangIndex : parser.hash.length) - 1),
        /**
        * Fragment parameters. Recognizes any of the following syntax:
        * #key1=value1&key2=value2
        * #id!key1=value1&key2=value2
        */
        fragmentparams: fromQueryString(parser.hash.substr(Math.max(0, hashBangIndex)))
    };
}

/**
 * Determines whether navigating to a URL would entail only a hash change.
 * @param url A URL string.
 * @return True if changing the location would trigger only an onhashchange event.
 */
export function isHashChange(url: string): boolean {
    let actual = parseURL(url);
    let expected = parseURL(location.href);  // parse location URL for compatibility with Internet Explorer

    return actual.protocol === expected.protocol
        && actual.host === expected.host
        && actual.pathname === expected.pathname  // compare path
        && actual.search === expected.search;     // compare query string
}

/**
 * Builds a query string from an object.
 * @param parameters An object where keys are parameter names, and values are parameter values.
 * @return A URL query string.
 */
export function buildQuery(parameters: { [key: string]: string }): string {
    return Object.keys(parameters).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(parameters[key] || '');
    }).join('&');
}
