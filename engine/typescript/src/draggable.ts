import {
    isFormControl,
} from './htmldom';

/** Allows viewing obscured parts of a scrollable element by making drag gestures with the mouse. */
export default class Draggable {
    private dragged: boolean = false;
    private lastClientX: number = 0;
    private lastClientY: number = 0;
    private static readonly scrollablePropertyValues = ['auto', 'scroll'];
    private scrollable: HTMLElement;

    /**
     * @param interceptor The element that intercepts drag events.
     * @param scrollable The element that scrolls in response to mouse movement.
     */
    constructor(interceptor: HTMLElement, scrollable: HTMLElement) {
        this.scrollable = scrollable;
        interceptor.addEventListener('mousedown', ev => this.dragStart(ev));
        interceptor.addEventListener('mouseup', ev => this.dragEnd(ev));
        interceptor.addEventListener('mouseout', ev => this.dragEnd(ev));
        interceptor.addEventListener('mousemove', ev => this.dragMove(ev));
    }

    private dragStart(event: Event) {
        if (isFormControl(event.target as Element)) {
            return;
        }

        let style = window.getComputedStyle(this.scrollable);
        let canScroll = Draggable.scrollablePropertyValues.indexOf(style['overflowX']) >= 0 || Draggable.scrollablePropertyValues.indexOf(style['overflowY']) >= 0;
        if (canScroll) {
            let mouseEvent = event as MouseEvent;
            this.lastClientX = mouseEvent.clientX;
            this.lastClientY = mouseEvent.clientY;
            this.dragged = true;
            mouseEvent.preventDefault();
        }
    }

    private dragEnd(_: Event) {
        this.dragged = false;
    }

    private dragMove(event: Event) {
        if (this.dragged) {
            let mouseEvent = event as MouseEvent;
            this.scrollable.scrollLeft -= mouseEvent.clientX - this.lastClientX;
            this.scrollable.scrollTop -= mouseEvent.clientY - this.lastClientY;
            this.lastClientX = mouseEvent.clientX;
            this.lastClientY = mouseEvent.clientY;
        }
    }
}
