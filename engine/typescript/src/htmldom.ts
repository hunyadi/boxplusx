/**
 * Determines whether an element is either of the listed HTML element types.
 * @param elem The HTML element to test.
 * @param types The tag names to test against.
 */
function isElementOfType(elem: Element, types: string[]): boolean {
    return types.indexOf(elem.tagName.toLowerCase()) >= 0;
}

export function isFormControl(elem: Element): boolean {
    return isElementOfType(elem, ['input', 'select', 'textarea']);
}
