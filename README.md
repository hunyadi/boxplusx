# boxplusx #

boxplusx is a lightbox pop-up window engine that aims to illustrate modern HTML visualization capabilities using plain JavaScript and CSS3. It borrows from traditional [boxplus](http://hunyadi.info.hu/levente/en/boxplus) but does not depend on third-party JavaScript libraries.

Features:

* Designed for modern browsers, including Chrome, Edge, Firefox and Safari.
* Displays a wide range of content types, including images, video, PDF, external pages (iframe), etc.
* Supports captions; each content item can be assigned a title and a description, which can make use of HTML tags.
* Navigation with buttons, mouse, keyboard (left and right arrow, HOME and END) or swipe.
* Fit to browser window; oversized content is automatically downscaled.
* Integrates with browser history support (the "back" button).
* Simple API and out-of-the-box snippets how to get it working on a webpage.
* 100% CSS and pure JavaScript (ECMAScript 5). No dependencies on third-party JavaScript libraries.
* Small network footprint.

### Examples ###

* [Live demo](http://hunyadi.info.hu/projects/boxplusx/)

### Related work ###

* The [original boxplus](http://hunyadi.info.hu/levente/en/boxplus)
