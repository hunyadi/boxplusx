document.addEventListener('DOMContentLoaded', function () {
    // a custom unique identifier that allows previous pop-up window instances to be found and disposed
    var uniqueId = 'boxplus-demo-container';

    var form = document.getElementById('options');

    function createPopUpWindow() {
        var options = {
            id: uniqueId
        };

        var params = ['slideshow','autostart','loop','navigation','controls','captions','contextmenu','metadata'];
        params.forEach(function (option) {
            var elem = form.elements[option];
            if (elem.type === 'checkbox') {
                options[option] = elem.checked;
            } else if (elem.type === 'number') {
                options[option] = parseInt(elem.value, 10);
            } else {
                options[option] = elem.value;
            }
        });

        var joomlausage = document.querySelector('#code-joomla code');
        joomlausage.innerText = '{gallery ' + params.map(function (option) {
            return 'lightbox:' + option + '=' + options[option];
        }).join(' ') + '} path/to/folder {/gallery}';
        hljs.highlightBlock(joomlausage);

        var jsusage = document.querySelector('#code-javascript code');
        jsusage.innerText = 'var dialog = new BoxPlusXDialog('+ JSON.stringify(options, null, 4) +');';
        hljs.highlightBlock(jsusage);

        var dialog = new BoxPlusXDialog(options);
        dialog.bind(document.querySelectorAll('.boxplusx-images'));
    }

    Array.prototype.forEach.call(form.elements, function (element) {
        var changeFn = function () {
            // delete an instance created earlier
            var container = document.getElementById(uniqueId);
            if (container) {
                container.parentNode.removeChild(container);
            }

            // create new instance with options currently set
            createPopUpWindow();
        };
        element.addEventListener('change', changeFn, false);
    });

    // create a binding with default parameters, invoked when the page loads
    createPopUpWindow();
}, false);
